import {defaultArray} from '../utils/game';
import _ from 'lodash'

export const PUZZLE_INIT = 'PUZZLE_INIT'
export const PUZZLE_MOVE = 'PUZZLE_MOVE'
export const GAME_WIN = 'GAME_WIN'
export const GAME_RESET = 'GAME_RESET'

export const puzzleInit = () => {
  const localArray = window.localStorage.getItem('puzzleArray')
  let puzzleArray = []
  if(!localArray) {
    const shuffArr = _.shuffle(defaultArray)
    window.localStorage.setItem('puzzleArray',JSON.stringify(shuffArr))
    puzzleArray = shuffArr
  } else {
    puzzleArray = JSON.parse(window.localStorage.getItem('puzzleArray'))
  }
  const puzzleMoves = JSON.parse(window.localStorage.getItem('puzzleMoves')) || 0

  return {
    type: PUZZLE_INIT,
    puzzleArray,
    puzzleMoves
  }
}

export const puzzleMove = (puzzleArray, puzzleCells) => {
  if(puzzleArray) {
    window.localStorage.setItem('puzzleArray',JSON.stringify(puzzleArray))
    const puzzleMoves = JSON.parse(window.localStorage.getItem('puzzleMoves')) + 1
    window.localStorage.setItem('puzzleMoves', JSON.stringify(puzzleMoves))
    return {
      type: PUZZLE_MOVE,
      puzzleArray,
      puzzleCells,
      puzzleMoves
    }
  }
}

export const gameWin = () => {
  return {
    type: GAME_WIN
  }
}

export const gameReset = () => {
  const puzzleArray = _.shuffle(defaultArray)
  window.localStorage.clear()
  window.localStorage.setItem('puzzleArray',JSON.stringify(puzzleArray))
  return {
    type: GAME_RESET,
    puzzleArray
  }
}