import _ from 'lodash'

export const defaultArray = _.range(1,16)
defaultArray.push(false)


export const girdCords = (array, size) => {
  let top = -size, left = 0

  return array.map((val, i) => {
    left = left + size
    if(i%4 === 0) {
      top = top + size
      left = 0
    }

    return {
      number: (typeof val === 'object') ? val.number : val,
      left: left,
      top: top
    }
  })
}

export const cellColor = (number) => {
  return number % 2 ? '#1c8cee' : number === false ? '#222' : '#d01d56'
}

export const shuffledCords = (array, size) => {
  return girdCords(_.shuffle(array), size)
}

export const findEmptyCell = (cells) => _.find(cells, ['number', false])


export const isNeighbors = (cell, cells, size) => {
  const empty = findEmptyCell(cells)
  const rowDiff = ((cell.left/size)+1)-((empty.left/size)+1)
  const colDiff = ((cell.top/size)+1)-((empty.top/size)+1)

  return Math.abs(rowDiff) + Math.abs(colDiff) === 1

}

export const swapCells = (cell, cells) => {
  const empty = findEmptyCell(cells)

  const swaped = cells.map((val, i)=> {
    if (val.number === cell.number) {
      return {
        number: val.number,
        left: empty.left,
        top: empty.top,
      }
    }

    if (val.number === false) {
      return {
        number: false,
        left: cell.left,
        top: cell.top,
      }
    }
    return val
  })
  return swaped
}

export const swapArray = (cell, arr) => {
  const empty = arr.findIndex(e=>e===false)
  return arr.map((elem)=> {
    if(elem === cell.number) {
      return false
    }
    if(elem === arr[empty]) {
      return cell.number
    }
    return elem
  })
}