import {
  PUZZLE_INIT,
  PUZZLE_MOVE,
  GAME_RESET,
  GAME_WIN
} from "../actions/puzzle";

const initialState = {
  puzzleArray: [],
  puzzleMoves: 0
}

export default function (state = initialState, action) {
  switch (action.type) {
    case  PUZZLE_INIT:
      return {...state, puzzleArray: action.puzzleArray, puzzleMoves: action.puzzleMoves}
    case PUZZLE_MOVE:
      return {...state, puzzleArray: action.puzzleArray, puzzleCells: action.puzzleCells, puzzleMoves: action.puzzleMoves}
    case GAME_RESET:
      return {...state, puzzleArray: action.puzzleArray, puzzleCells: null, puzzleMoves: 0}
    case GAME_WIN:
      return {...action}
    default:
      return state
  }
}