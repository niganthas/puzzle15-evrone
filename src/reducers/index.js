import { combineReducers } from 'redux'
import puzzleReducer from './puzzle'

export default combineReducers({
  puzzle: puzzleReducer
})