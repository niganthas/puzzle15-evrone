import React from 'react'
import styled from 'styled-components'
import Cell from './Cell'

const Grid = ({className, cells, size, onCellClick}) => {

  const createGrid = () => {
    return cells.map((cords, i) =>
      <Cell
        key={i}
        size={size}
        cords={cords}
        onCellClick={onCellClick}
      />)
  }

  return (
      <div
          style={{
            width: size*4+4,
            height: size*4+4,
          }}
          className={className}>
        {createGrid()}
      </div>
  );
};

export default styled(Grid)`
  flex: 1;
  display: flex;

  position: relative;
 
`;
