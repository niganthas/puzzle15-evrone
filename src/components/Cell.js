import React from 'react';
import styled from 'styled-components'
import {cellColor} from '../utils/game'

const Cell = ({className, cords, onCellClick}) => {
  const styles = {
    transform: `translate(${cords.left}px, ${cords.top}px)`
  }

  return (
      <div className={className}
           onClick={() => onCellClick(cords)}
           style={styles}>
        <span>{cords.number}</span>
      </div>
  );
};

export default styled(Cell)`
  width: 25%;
  height: 25%;
  background: ${props => cellColor(props.cords.number)};
  color:#fff;
  font-size: 2em;
  position: absolute;
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  ${props => props.cords.number && 'z-index: 5;'}
  ${props => props.cords.number && 'cursor: pointer;'}
  box-sizing: border-box;
  border: 1px solid rgba(0,0,0,.2);
  transition: all .3s ease-in-out;
  
  &:hover {
    opacity: .8;
  }


`;
