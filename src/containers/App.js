import React, { Component } from 'react';
import { connect } from 'react-redux'
import {Grid} from '../components'
import {puzzleInit, puzzleMove, gameReset} from '../actions/puzzle';
import {isNeighbors, girdCords, swapCells, swapArray} from '../utils/game'

class App extends Component {

  state = {
    clientWidth: 1024,
    clientHeight: 768,
    size: 768/4,
    puzzleArray: [],
    cells: []
  }

  componentWillMount() {
    window.addEventListener('resize', this.onWindowResize.bind(this))
    const size = window.innerHeight/4
    this.setState({
      clientWidth: window.innerWidth,
      clientHeight: window.innerHeight,
      size: size,
    })
    this.props.puzzleInit()
  }


  componentWillReceiveProps(nextProps) {
    if(!nextProps.puzzleCells) {
      this.setState({
        puzzleArray: nextProps.puzzleArray,
        cells: girdCords(nextProps.puzzleArray, this.state.size)
        // cells: girdCords(nextProps.puzzleArray.puzzleArray, this.state.size)
      })
    } else {
      this.setState({
        puzzleArray: nextProps.puzzleArray,
        cells: nextProps.puzzleCells
      })
    }

  }


  componentWillUnmount() {
    window.removeEventListener('resize', this.onWindowResize.bind(this))
  }

  //Handlers
  onWindowResize(event) {
    const size = window.innerHeight/4
    this.setState({
      clientWidth: window.innerWidth,
      clientHeight: window.innerHeight,
      size: size,
      cells: girdCords(this.state.puzzleArray, size)
    })
  }

  onCellClickHandler(cell) {
    if(isNeighbors(cell, this.state.cells,  this.state.size)) {
      const swapedCells = swapCells(cell, this.state.cells)
      const swapedArray = swapArray(cell, this.state.puzzleArray)
      this.props.puzzleMove(swapedArray, swapedCells)
    }
  }

  onNewGame() {
    this.props.gameReset()
  }

  render() {
    const {size, cells} = this.state
    return (
      <div className="App">
        <Grid
            cells={cells}
            size={size}
            onCellClick={(cell, empty)=>this.onCellClickHandler(cell, empty)}
        />
        <div className={'table'}>
          Moves: {this.props.puzzleMoves}
          <div className={'new-game'} onClick={e=>this.onNewGame()}>New game</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  puzzleArray: state.puzzle.puzzleArray,
  puzzleCells: state.puzzle.puzzleCells,
  puzzleMoves: state.puzzle.puzzleMoves,
})


const mapDispatchToProps = {
  puzzleInit,
  puzzleMove,
  gameReset
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
