import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'


const configureStore = (preloadedState, browserHistory) => {
  const store = createStore(
      rootReducer,
      compose(
          applyMiddleware(),
       //   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
      )
  )


  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(rootReducer)
    })
  }

  return store
}

export default configureStore